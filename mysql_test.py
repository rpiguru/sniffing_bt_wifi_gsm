import MySQLdb

from base import Base


ctrl = Base()

host_name = ctrl.get_param_from_xml("MYSQL_HOST") + ":3306"
db_name = ctrl.get_param_from_xml("MYSQL_DB")
tb_name = ctrl.get_param_from_xml("MYSQL_TABLE")
user_name = ctrl.get_param_from_xml("MYSQL_USER")
pwd = ctrl.get_param_from_xml("MYSQL_PWD")

try:
    db = MySQLdb.connect(host=host_name, user=user_name, passwd=pwd, db=db_name)

    cur = db.cursor()

    sql = "CREATE TABLE IF NOT EXISTS '" + tb_name + "' (" + \
          "DeviceID INT(11) NOT NULL AUTO_INCREMENT, " + \
          "WifiMacAddress VARCHAR(45) DEFAULT NULL, " + \
          "WifiTimestamp DATETIME DEFAULT NULL, " + \
          "WifiSignalStrength VARCHAR(45) DEFAULT NULL, " + \
          "WifiDistance VARCHAR(45) DEFAULT NULL, " + \
          "BTMacAddress VARCHAR(45) DEFAULT NULL, " + \
          "BTSignalStrength VARCHAR(45) DEFAULT NULL, " + \
          "BTDistance VARCHAR(45) DEFAULT NULL, " + \
          "BTTimestamp DATETIME DEFAULT NULL, " + \
          "PRIMARY KEY (DeviceID)) ENGINE=InnoDB"
    print "Creating SQL: ", sql
    cur.execute(sql)
    db.commit()

    cur.execute("SELECT * FROM " + tb_name)

    # print all the first cell of all the rows
    for row in cur.fetchall():
        print row[0]

    cur.close()
    db.close()

except MySQLdb.Error as error:
    print("Error %s:" % error)



