import glob
import time
import os
import copy
import datetime

import math
from kivy.adapters.listadapter import ListAdapter
from kivy.adapters.models import SelectableDataItem
from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty, BooleanProperty, ListProperty
from kivy.uix.listview import ListItemButton, ListView
from kivy.uix.screenmanager import Screen
from kivy.uix.popup import Popup
from functools import partial
from kivy.uix.scrollview import ScrollView
from kivy.clock import Clock
from kivy.core.window import Window
from base import Base

debug = 0

if debug == 0:
    from bt_ctrl import BTCtrl
    from wifi_ctrl import WiFiCtrl


class CautionPopup(Popup):
    pass


class ConfirmPopup(Popup):
    pass


class MainScreen(Screen):
    fullscreen = BooleanProperty(False)

    def add_widget(self, *args):
        if 'content' in self.ids:
            return self.ids.content.add_widget(*args)
        return super(MainScreen, self).add_widget(*args)


class ScrollableLabel(ScrollView):
    text = StringProperty('')


class DataItem(SelectableDataItem):
    def __init__(self, name, **kwargs):
        self.name = name
        super(DataItem, self).__init__(**kwargs)


class MainApp(App, Base):
    # variables for GUI
    current_title = StringProperty()
    screen_names = []
    screens = {}
    hierarchy = []
    caution_popup = None
    confirm_popup = None

    popup_param = None  # contains parameters for confirm_popup (callback string, numbers, etc)

    alive_time = 0
    bt_ctrl = None
    wifi_ctrl = None

    bt_list = [[], [], [], [], []]      # mac, rssi, time, datetime, distance
    wifi_list = [[], [], [], [], []]    # mac, rssi, time, datetime, distance
    bt_list_adapter = None
    wifi_list_adapter = None
    bt_list_view = None
    wifi_list_view = None

    cal_type = None
    cal_addr = None
    cal_distance = None
    a_val = 0
    n_val_list = []

    def build(self):
        """
        base function of kivy app
        :return:
        """
        if debug == 0:
            self.alive_time = int(self.get_param_from_xml('ALIVE_TIME'))
            print "aaa"
        else:
            Window.size = (800, 480)

        self.load_screen()
        self.caution_popup = CautionPopup()
        self.confirm_popup = ConfirmPopup()
        if debug > 0:
            self.go_screen('dashboard', 'right')
        else:
            self.go_screen('welcome', 'right')

        # Initialize listview controller
        list_item_args_converter = lambda row_index, obj: {'text': obj.name, 'size_hint_y': None, 'height': 40}

        dd = []
        if debug > 0:
            for i in range(30):
                dd.append(DataItem("MAC: 00:36:76:50:2E:57,   Distance: " + str(i) + "m"))

        self.wifi_list_adapter = ListAdapter(data=dd, args_converter=list_item_args_converter, selection_mode='single',
                                             propagate_selection_to_data=False, allow_empty_selection=False,
                                             cls=ListItemButton)
        self.wifi_list_view = ListView(adapter=self.wifi_list_adapter, pos=(60, 120), size_hint=(0.4, 0.5))
        self.screens['dashboard'].add_widget(self.wifi_list_view)

        self.bt_list_adapter = ListAdapter(data=dd, args_converter=list_item_args_converter, selection_mode='single',
                                           propagate_selection_to_data=False, allow_empty_selection=False,
                                           cls=ListItemButton)
        self.bt_list_view = ListView(adapter=self.bt_list_adapter, pos=(420, 120), size_hint=(0.4, 0.5))
        self.screens['dashboard'].add_widget(self.bt_list_view)

    def btn_start(self):
        """
        This function is called when user presses 'START' button.
        :return:
        """
        self.confirm_popup.ids['lb_confirm'].text = "Would you like to start now?"
        self.popup_param = ('start',)
        self.confirm_popup.open()

    def wifi_calibrate(self):
        if len(self.wifi_list_adapter.selection) == 0:
            return False
        dev = self.wifi_list_adapter.selection[0]
        # get mac address
        addr = dev.text.split(' ')[1]
        addr = addr[:-1]
        self.cal_type = 'wifi'
        self.confirm_popup.ids['lb_confirm'].text = "WiFi MAC address is " + addr + ".\nIs this correct?"
        self.popup_param = ('cal', 'wifi', addr)
        self.confirm_popup.open()

    def bt_calibrate(self):
        if len(self.bt_list_adapter.selection) == 0:
            return False
        dev = self.bt_list_adapter.selection[0]
        # get mac address
        addr = dev.text.split(' ')[1]
        addr = addr[:-1]
        self.cal_type = 'bt'
        self.confirm_popup.ids['lb_confirm'].text = "Bluetooth MAC address is " + addr + ".\nIs this correct?"
        self.popup_param = ('cal', 'bt', addr)
        self.confirm_popup.open()

    def confirm_yes(self):
        """
        callback function of "Yes" button in confirm popup
        :return:
        """
        self.confirm_popup.dismiss()
        if self.popup_param:
            if self.popup_param[0] == 'start':
                self.prepare()
            elif self.popup_param[0] == 'cal':
                self.n_val_list = []
                Clock.unschedule(self.display_devices)
                self.cal_type = self.popup_param[1]
                self.cal_addr = self.popup_param[2]
                self.initialize_calibration()
                self.go_screen('calibration', 'right')
            elif self.popup_param[0] == 'cal_individual':
                self.procedure_cal(self.popup_param[1])

    def prepare(self):
        """
        Prepare Bluetooth and WiFi for sniffing
        Initialize both of modules and show the result
        If success, go to the status screen
        :return:
        """
        print "Initializing Bluetooth..."
        self.root.ids['lb_status'].text = "Initializing Bluetooth..."
        self.bt_ctrl = BTCtrl()
        if not self.bt_ctrl.initialize():
            msg = "Error accessing bluetooth device..."
            self.show_caution_popup(msg)
            return False

        # Initialize Bluetooth. See set_inquiry_mode function for detail.
        r = self.bt_ctrl.set_inquiry_mode()
        if r == 1:
            msg = "Error reading inquiry mode.\nAre you sure this is a bluetooth 1.2 device?"
            self.show_caution_popup(msg)
            return False
        elif r == 2:
            msg = "Error writing inquiry mode.  Are you sure you're root?"
            self.show_caution_popup(msg)
            return False
        elif r == 3:
            msg = "Error while setting inquiry mode"
            self.show_caution_popup(msg)
            return False
        elif r == 4:  # when succeeded to set inquiry mode, initialize again.
            self.bt_ctrl.set_inquiry_mode()

        print "Initializing WiFi..."
        self.root.ids['lb_status'].text = "Initializing WiFi..."
        self.wifi_ctrl = WiFiCtrl()
        if not self.wifi_ctrl.initialize():
            msg = "Failed to initialize WiFi...\nPlease plug in wifi dongle and reboot Raspberry Pi."
            self.show_caution_popup(msg)
            return False
        self.start_scanning(0)

    def start_scanning(self, param):
        """
        Move to the dashboard screen and start scanning
        :param param: parameter for deciding other stuff
            when 0 :    this function is called from the start button on welcome page
            when 1 :    this function is called from the back button on calibration page
        """
        self.go_screen('dashboard', 'left')
        self.bt_ctrl.set_dedicated(None)
        self.wifi_ctrl.set_dedicated(None)
        self.n_val_list = []

        if param == 1:
            Clock.unschedule(self.calibrate)
            self.bt_ctrl.resume()
            self.wifi_ctrl.resume()
        else:
            self.bt_ctrl.start()
            self.wifi_ctrl.start()

        self.root.ids['lb_status'].text = "Scanning WiFi and Bluetooth devices..."
        Clock.schedule_interval(self.display_devices, 1)    # display date & time
        # Clock.schedule_interval(self.print_status, 3)       # display status of app,   for debug

    def show_caution_popup(self, message):
        """
        Show caution popup window with given message
        :param message:
        :return:
        """
        self.caution_popup.ids['lb_content'].text = message
        self.caution_popup.open()

    def display_devices(self, *args):
        """
        get detected wifi/bt devices from the Queue and update, display
        :param args:
        :return:
        """
        self.update_wifi_list()
        self.update_bt_list()

        cnt_wifi = len(self.wifi_list[0])
        cnt_bt = len(self.bt_list[0])
        print cnt_bt, "--------", cnt_wifi
        # get count of same device

        def get_diff_count(a, b):
            re = 0
            u = zip(a, b)
            for i, j in u:
                if i != j:
                    re += 1
            return re

        uniq_num = 0
        for bt_d in self.bt_list[0]:
            for wifi_d in self.wifi_list[0]:
                if get_diff_count(bt_d, wifi_d) < 3:
                    uniq_num += 1
                    break

        cnt_total = cnt_bt + cnt_wifi - uniq_num
        msg = "WiFi : " + str(cnt_wifi) + "     Bluetooth : " + str(cnt_bt) + "     Total : " + str(cnt_total)
        print msg
        lb_widget = self.screens['dashboard'].ids.lb_total
        lb_widget.text = msg

    def update_wifi_list(self):
        queue_size = self.wifi_ctrl.wifi_queue.qsize()
        if queue_size > 0:
            print "Detected WiFi device: ", queue_size

            dev = self.wifi_ctrl.wifi_queue.get()
            sorted_wifi_list = []
            if queue_size == 1:
                sorted_wifi_list.append(dev)
            else:
                # Get the strongest RSSI in the queue
                # Because there are several packets sent from same device with different RSSI.
                mac_buf = dev[0]
                rssi_buf = dev[1]

                for i in range(queue_size - 1):
                    d = self.wifi_ctrl.wifi_queue.get()
                    if d[0] == mac_buf:
                        if d[1] > rssi_buf:
                            rssi_buf = d[1]
                    else:
                        sorted_wifi_list.append((mac_buf, rssi_buf))
                        mac_buf = d[0]
                        rssi_buf = d[1]
                sorted_wifi_list.append((mac_buf, rssi_buf))  # add final device to the list

            for wifi_dev in sorted_wifi_list:
                dist = self.get_distance_from_rssi(wifi_dev[0], wifi_dev[1], 'wifi')
                if wifi_dev[0] not in self.wifi_list[0]:
                    self.wifi_list[0].append(wifi_dev[0])               # append mac address
                    self.wifi_list[1].append(wifi_dev[1])               # append rssi
                    self.wifi_list[2].append(time.time())               # append time
                    self.wifi_list[3].append(datetime.datetime.now())   # append datetime
                    self.wifi_list[4].append(dist)                      # append distance
                    self.insert_wifi_dev(wifi_dev, dist)  # append device to the list view
                else:
                    num = self.wifi_list[0].index(wifi_dev[0])  # get count number of this device
                    self.wifi_list[1][num] = wifi_dev[1]                    # update rssi
                    self.wifi_list[2][num] = time.time()                    # update time
                    self.wifi_list[3][num] = datetime.datetime.now()        # update datetime
                    # update distance
                    self.wifi_list[4][num] = dist
                    self.update_wifi_device(wifi_dev, num, dist)

        # kill devices which is older than alive_time
        for i in range(len(self.wifi_list[0])):
            if time.time() - self.wifi_list[2][i] > self.alive_time:
                for j in range(5):
                    self.wifi_list[j].pop(i)
                self.wifi_list_adapter.data.pop(i)
                break

    def update_bt_list(self):
        queue_size = self.bt_ctrl.bt_queue.qsize()
        if queue_size > 0:
            print "Detected BT device: ", queue_size

            dev = self.bt_ctrl.bt_queue.get()
            sorted_bt_list = []
            if queue_size == 1:
                sorted_bt_list.append(dev)
            else:
                # Get the strongest RSSI in the queue
                # Because there are several packets sent from same device with different RSSI.
                mac_buf = dev[0]
                rssi_buf = dev[1]

                for i in range(queue_size - 1):
                    d = self.bt_ctrl.bt_queue.get()
                    if d[0] == mac_buf:
                        if d[1] > rssi_buf:
                            rssi_buf = d[1]
                    else:
                        sorted_bt_list.append((mac_buf, rssi_buf))
                        mac_buf = d[0]
                        rssi_buf = d[1]
                sorted_bt_list.append((mac_buf, rssi_buf))  # add final device to the list

            for bt_dev in sorted_bt_list:
                dist = self.get_distance_from_rssi(bt_dev[0], bt_dev[1], 'bt')
                if bt_dev[0] not in self.bt_list[0]:
                    self.bt_list[0].append(bt_dev[0])  # append mac address
                    self.bt_list[1].append(bt_dev[1])  # append rssi
                    self.bt_list[2].append(time.time())  # append time
                    self.bt_list[3].append(datetime.datetime.now())  # append datetime
                    self.bt_list[4].append(dist)
                    self.insert_bt_dev(bt_dev, dist)  # append device to the list view
                else:
                    num = self.bt_list[0].index(bt_dev[0])  # get count number of this device
                    self.bt_list[1][num] = bt_dev[1]  # update rssi
                    self.bt_list[2][num] = time.time()  # update time
                    self.bt_list[3][num] = datetime.datetime.now()  # update datetime
                    # update distance
                    self.bt_list[4][num] = dist
                    self.update_bt_device(bt_dev, num, dist)

        for i in range(len(self.bt_list[0])):
            if time.time() - self.bt_list[2][i] > self.alive_time:
                for j in range(5):
                    self.bt_list[j].pop(i)
                self.bt_list_adapter.data.pop(i)
                break

    def get_distance_from_rssi(self, addr, r, dev_type):
        """
        Calculate distance from the rssi
        d = 10 ^ ((A - rssi) / 10 / n)
        A : RSSI value when the device is 1m away
        n : parameter, must be calibrated.

        :param addr: Mac address of device
        :param r: rssi value
        :return: If device is not calibrated yet, returns None
        """
        # get A-value from the db
        dev = self.get_dev(addr, dev_type)
        if dev:
            a_val = dev[3]
            n_val = dev[4]
            d = pow(10, (a_val - r) / 10.0 / n_val)
            return d
        else:
            return None

    def insert_wifi_dev(self, dev, distance):
        """
        Insert Device in the list view
        :param dev: (MAC, RSSI)
        :param distance: distance
        :return:
        """
        print "Inserting wifi device..."

        items = self.wifi_list_adapter.data
        if distance:
            txt = "MAC: %s,   Distance: %0.2f m" % (dev[0], distance)
        else:
            txt = "MAC: %s,   Distance: N/A" % dev[0]

        items.append(DataItem(txt))

    def insert_bt_dev(self, dev, distance):
        """
        Insert Device in the list view
        :param dev: (MAC, RSSI)
        :param distance: distance
        :return:
        """

        items = self.bt_list_adapter.data
        if distance:
            txt = "MAC: %s,   Distance: %0.2f m" % (dev[0], distance)
        else:
            txt = "MAC: %s,   Distance: N/A" % dev[0]

        items.append(DataItem(txt))

    def update_wifi_device(self, dev, num, distance):
        """
        Update the device's RSSI in the wifi list view
        :param num:
        :param distance: distance
        :return:
        """
        items = self.wifi_list_adapter.data

        if distance:
            txt = "MAC: %s,   Distance: %0.2f m" % (dev[0], distance)
        else:
            txt = "MAC: %s,   Distance: N/A" % dev[0]
        items[num] = DataItem(txt)

    def update_bt_device(self, dev, num, distance):
        """
        Update the device's RSSI in the bt list view
        :param num:
        :param distance: distance
        :return:
        """
        items = self.bt_list_adapter.data

        if distance:
            txt = "MAC: %s,   Distance: %0.2f m" % (dev[0], distance)
        else:
            txt = "MAC: %s,   Distance: N/A" % dev[0]
        items[num] = DataItem(txt)

    def go_screen(self, dest_screen, direction):
        """
        Go to given screen
        :param dest_screen:     destination screen name
        :param direction:       "up", "down", "right", "left"
        :return:
        """
        screen = self.screens[dest_screen]
        sm = self.root.ids.sm
        sm.switch_to(screen, direction=direction)

    def caution_ok(self):
        self.caution_popup.dismiss()
        if self.popup_param[0] == 'close':
            app.stop()

    def load_screen(self):
        """
        Load all screens from data/screens to Screen Manager
        :return:
        """
        available_screens = []
        if debug == 0:
            full_path_screens = glob.glob("/home/pi/pos_diamond/data/screens/*.kv")
        else:
            full_path_screens = glob.glob("data/screens/*.kv")
        for file_path in full_path_screens:
            file_name = os.path.basename(file_path)
            available_screens.append(file_name.split(".")[0])

        self.screen_names = available_screens
        for i in range(len(full_path_screens)):
            screen = Builder.load_file(full_path_screens[i])
            self.screens[available_screens[i]] = screen
        return True

    def initialize_calibration(self):
        """
        Initialize the Calibration.
        """
        self.n_val_list = []
        self.root.ids['lb_status'].text = "Calibrating device...     Please be patient, this will take a while."
        self.bt_ctrl.stop()
        self.wifi_ctrl.stop()

        if self.cal_type == 'bt':
            # pop all previous devices from the queue
            while self.bt_ctrl.bt_queue.qsize() > 0:
                print self.bt_ctrl.bt_queue.get()
            self.bt_ctrl.set_dedicated(self.cal_addr)
            tmp_buf = 'Bluetooth'
        else:
            # pop all previous devices from the queue
            while self.wifi_ctrl.wifi_queue.qsize() > 0:
                print self.wifi_ctrl.wifi_queue.get()
            self.wifi_ctrl.set_dedicated(self.cal_addr)
            tmp_buf = 'WiFi'

        screen = self.screens['calibration']
        screen.ids.lb_title.text = '[size=30]Type: ' + tmp_buf + ",  MAC Address: " + self.cal_addr
        for j in ['3', '5', '10']:
            screen.ids['btn_cal_' + j].disabled = True
            screen.ids['btn_cal_' + j].text = "Not started"
        screen.ids.btn_cal_1.disabled = False
        screen.ids.btn_cal_1.text = "Calibrate in 1m"

    def btn_calibrate(self, distance):
        msg = "Is your device away %dm from the RPi?" % distance
        self.confirm_popup.ids['lb_confirm'].text = msg
        self.popup_param = ('cal_individual', distance)
        self.confirm_popup.open()

    def procedure_cal(self, distance):
        print "Calibrating device(mac: ", self.cal_addr, ") in ", distance, "m"
        self.cal_distance = distance
        self.bt_ctrl.resume()
        self.wifi_ctrl.resume()
        self.screens['calibration'].ids['btn_cal_' + str(distance)].text = 'Calibrating...'
        self.screens['calibration'].ids['btn_cal_' + str(distance)].disbled = True
        Clock.schedule_interval(self.calibrate, 1)

    def calibrate(self, *args):
        """
        Calibrate function which is called every second.
        """

        rssi_list = []
        if self.cal_type == 'bt':
            queue_size = self.bt_ctrl.bt_queue.qsize()
            print "Detect count: ", queue_size
            if queue_size >= 5:
                self.bt_ctrl.stop()
                for i in range(queue_size):
                    rssi_list.append(self.bt_ctrl.bt_queue.get()[1])
            else:
                return True
        else:
            queue_size = self.wifi_ctrl.wifi_queue.qsize()
            print "Detect count: ", queue_size
            if queue_size >= 5:
                self.wifi_ctrl.stop()
                for i in range(queue_size):
                    rssi_list.append(self.wifi_ctrl.wifi_queue.get()[1])
            else:
                return True

        # pop minimum, maximum value among the 5 values
        rssi_list.sort()
        rssi_list.pop(0)
        rssi_list.pop(-1)

        screen = self.screens['calibration']
        if self.cal_distance == 1:
            self.a_val = (rssi_list[0] + rssi_list[1] + rssi_list[2]) / 3.0
            print "A: ", self.a_val
            screen.ids.btn_cal_1.disabled = True
            screen.ids.btn_cal_1.text = "Done"
            screen.ids.btn_cal_3.disabled = False
            screen.ids.btn_cal_3.text = "Calibrate in 3m"
        else:
            for r in rssi_list:
                n = (self.a_val - r) / 10.0 / math.log(self.cal_distance, 10)
                print "N: ", n
                self.n_val_list.append(n)
            if self.cal_distance == 3:
                screen.ids.btn_cal_3.disabled = True
                screen.ids.btn_cal_3.text = "Done"
                screen.ids.btn_cal_5.disabled = False
                screen.ids.btn_cal_5.text = "Calibrate in 5m"
            elif self.cal_distance == 5:
                screen.ids.btn_cal_5.disabled = True
                screen.ids.btn_cal_5.text = "Done"
                screen.ids.btn_cal_10.disabled = False
                screen.ids.btn_cal_10.text = "Calibrate in 10m"
            elif self.cal_distance == 10:
                screen.ids.btn_cal_10.text = "Done"
                screen.ids.btn_cal_10.disabled = True

        if self.cal_distance == 10:
            # get average value and update db

            avg_n = reduce(lambda x, y: x + y, self.n_val_list) / len(self.n_val_list)
            print "Final n: ", avg_n
            print "A : ", self.a_val
            if avg_n < 2 or avg_n > 4:
                avg_n = 2

            if self.get_distance_from_rssi(self.cal_addr, 1, self.cal_type) is None:
                sql = "INSERT INTO tb_dev (mac, dev_type, val_A, val_n) values('" + self.cal_addr + "', '" + \
                      self.cal_type + "', " + str(self.a_val) + ", " + str(avg_n) + ");"
            else:
                sql = "UPDATE tb_dev set val_A=" + str(self.a_val) + ", val_n=" + str(avg_n) + \
                      " WHERE mac='" + self.cal_addr + "' and dev_type='" + self.cal_type + "';"
            self.curs.execute(sql)
            self.conn.commit()

            msg = "Congratulations. \nYou have finished calibration on " + self.cal_addr + "."

        else:
            msg = "Calibration in " + str(self.cal_distance) + "m done. Now, please go ahead."

        self.show_caution_popup(msg)

        Clock.unschedule(self.calibrate)

    def upload_data(self):
        self.wifi_ctrl.terminate()
        self.wifi_ctrl.pause_monitoring()
        self.root.ids['lb_status'].text = "Uploading data to the mysql server"
        # TODO: Insert uploading logic here

        self.wifi_ctrl.initialize()
        self.wifi_ctrl.set_dedicated(None)
        self.bt_ctrl.start()

    def close(self):
        self.bt_ctrl.terminate()
        self.wifi_ctrl.terminate()
        os.system("sudo ifconfig $iface down")
        os.system("sudo iw dev $iface del")
        app.stop()

    def restart(self):
        os.system("sudo reboot")

    def print_status(self, *args):
        print "WiFi controller: ", self.wifi_ctrl.stopped()
        print "BT controller: ", self.bt_ctrl.stopped()

if __name__ == '__main__':
    app = MainApp()
    app.run()
