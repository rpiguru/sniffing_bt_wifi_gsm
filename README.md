# Install dependencies #

## Preparing Raspberry Pi ##
Open the terminal and expand file system in the raspi-config
    
    sudo raspi-config
Select `Option 1` and expand file system and reboot Raspberry Pi.

After rebooting, update and upgrade all packages.
    
    sudo apt-get update
    sudo apt-get upgrade -y
    
### Install BT Driver ###

    sudo apt-get install python-pip python-dev ipython
    sudo apt-get install bluetooth libbluetooth-dev
    sudo pip install pybluez
    
###  Install for WiFi ###
    
    sudo apt-get install firmware-linux-free
    sudo apt-get install libpcap-dev
    sudo apt-get install python-pcapy
    sudo apt-get install hostapd dnsmasq
    sudo apt-get install sqlite3
    sudo apt-get install python-mysqldb
    
Restart Raspberry Pi

    sudo reboot
    
## Testing BT/WiFi sniffer ##

Copy source files to `/home/pi/pos_diamond`.

###  Run BT sniffer ###

    cd test/bt
    sudo python bt_scan.py

###  Run WiFi sniffer ###

Make sure that NetGear WiFi dongle is plugged in.

* Switch to AP mode
    
        cd test/wifi
        sudo ./ap_setup.sh
        sudo ifdown wlan0
        sudo /usr/sbin/hostapd /etc/hostapd/hostapd.conf
    
After this, RPi's built-in WiFi will work as AP from the next boot time.

To enable internet on `wlan1`, open the `/etc/network/interface` file and edit something.
    
    sudo nano /etc/network/interface
Add following to the wlan1 section
    
    allow-hotplug wlan1
    auto wlan1
    iface wlan1 inet dhcp
        wpa-ssid "Your Router's AP name"
        wpa-psk "Your Router's AP password"

* Run sniffer
        
        cd test/wifi/wuds
        sudo ./run.sh

# Install Kivy Framework
* Install the dependencies:
    
        sudo apt-get install libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev \
        pkg-config libgl1-mesa-dev libgles2-mesa-dev \
        python-setuptools libgstreamer1.0-dev git-core \
        gstreamer1.0-plugins-{bad,base,good,ugly} \
        gstreamer1.0-{omx,alsa} python-dev cython

* Install Kivy globally:

        sudo pip install git+https://github.com/kivy/kivy.git@master
        
* Using Official RPi touch display

    If you are using the official Raspberry Pi touch display, you need to configure Kivy to use it as an input source.
    
    To do this, edit the file `~/.kivy/config.ini` and go to the `[input]` section. 
    
    Add this:
        
        mouse = mouse
        mtdev_%(name)s = probesysfs,provider=mtdev
        hid_%(name)s = probesysfs,provider=hidinput
        
* Since we are using official 7" touch screen, we need to change something.
       
        nano /home/pi/.kivy/config.ini
    
    Go to the `[input]` section and add this:
  
        mouse = mouse
        mtdev_%(name)s = probesysfs,provider=mtdev
        hid_%(name)s = probesysfs,provider=hidinput

    Change some values
    
        keyboard_mode = dock
        fullscreen = 1
        height = 480
        width = 800
    
# Enable auto-start #

Run the main script in the root mode.
    
    cd /home/pi/pos_diamond
    sudo python main.py
    
You can see that resolution is not match, but no matter.

Copy configuration file to the root's profile directory.
    
    sudo cp /home/pi/.kivy/config.ini /root/.kivy/config.ini
    
Create new bash script to start automatically.
    
    sudo nano /usr/local/bin/kivy_run.sh

Insert below in the created file.
    
    #!/bin/bash
    source /home/pi/.profile
    /usr/bin/python /home/pi/pos_diamond/main.py
Press CTRL +X, Y to save and exit.

Make created file executable.
    
    sudo chmod +x /usr/local/bin/kivy_run.sh

Open the rc.local
    
    sudo nano /etc/rc.local

Insert before exit 0  to start automatically.
    
    /usr/bin/xinit /usr/local/bin/kivy_run.sh &       
        
        
    
    
    
