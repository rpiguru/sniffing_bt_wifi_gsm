import sys
import struct
from Queue import Queue

import bluetooth._bluetooth as bluez
import bluetooth
import time

from base import Base


class BTCtrl(Base):

    sock = None
    mode = 0
    _bt_queue = None

    def __init__(self):
        Base.__init__(self)
        self._bt_queue = Queue()

    def initialize(self):
        """
        Create BT socket instance
        :return:
        """
        try:
            self.sock = bluez.hci_open_dev(0)
            return True
        except Exception as e:
            print("error accessing bluetooth device...")
            print e
            return False

    @property
    def bt_queue(self):
        return self._bt_queue

    def set_inquiry_mode(self):
        """
        Set BT inquiry mode and return result
        :return:
        """
        try:
            self.mode = self.read_inquiry_mode()
        except Exception as e:
            print("Error reading inquiry mode.  ")
            print("Are you sure this a bluetooth 1.2 device?")
            print(e)
            return 1

        print "BT inquiry mode: ", self.mode
        if self.mode != 1:
            print("writing inquiry mode...")
            try:
                result = self.write_inquiry_mode()
            except Exception as e:
                print("error writing inquiry mode.  Are you sure you're root?")
                print(e)
                return 2
            print 'Setting result: ', result

            if result != 0:
                print("error while setting inquiry mode")
                return 3
            else:
                return 4
        else:
            return 0

    def printpacket(self, pkt):
        for c in pkt:
            sys.stdout.write("%02x " % struct.unpack("B", c)[0])
        print ""

    def read_inquiry_mode(self):
        """
        returns the current mode, or -1 on failure
        """

        # save current filter
        old_filter = self.sock.getsockopt(bluez.SOL_HCI, bluez.HCI_FILTER, 14)

        # Setup socket filter to receive only events related to the
        # read_inquiry_mode command
        flt = bluez.hci_filter_new()
        opcode = bluez.cmd_opcode_pack(bluez.OGF_HOST_CTL, bluez.OCF_READ_INQUIRY_MODE)
        bluez.hci_filter_set_ptype(flt, bluez.HCI_EVENT_PKT)
        bluez.hci_filter_set_event(flt, bluez.EVT_CMD_COMPLETE)
        bluez.hci_filter_set_opcode(flt, opcode)
        self.sock.setsockopt(bluez.SOL_HCI, bluez.HCI_FILTER, flt)

        # first read the current inquiry mode.
        bluez.hci_send_cmd(self.sock, bluez.OGF_HOST_CTL, bluez.OCF_READ_INQUIRY_MODE)

        pkt = self.sock.recv(255)

        status, _mode = struct.unpack("xxxxxxBB", pkt)
        if status != 0:
            _mode = -1

        # restore old filter
        self.sock.setsockopt(bluez.SOL_HCI, bluez.HCI_FILTER, old_filter)
        return _mode

    def write_inquiry_mode(self):
        """
        returns 0 on success, -1 on failure
        """
        # save current filter
        old_filter = self.sock.getsockopt(bluez.SOL_HCI, bluez.HCI_FILTER, 14)

        # Setup socket filter to receive only events related to the
        # write_inquiry_mode command
        flt = bluez.hci_filter_new()
        opcode = bluez.cmd_opcode_pack(bluez.OGF_HOST_CTL, bluez.OCF_WRITE_INQUIRY_MODE)
        bluez.hci_filter_set_ptype(flt, bluez.HCI_EVENT_PKT)
        bluez.hci_filter_set_event(flt, bluez.EVT_CMD_COMPLETE)
        bluez.hci_filter_set_opcode(flt, opcode)
        self.sock.setsockopt(bluez.SOL_HCI, bluez.HCI_FILTER, flt)

        # send the command!
        bluez.hci_send_cmd(self.sock, bluez.OGF_HOST_CTL, bluez.OCF_WRITE_INQUIRY_MODE, struct.pack("B", 1))

        pkt = self.sock.recv(255)

        status = struct.unpack("xxxxxxB", pkt)[0]

        # restore old filter
        self.sock.setsockopt(bluez.SOL_HCI, bluez.HCI_FILTER, old_filter)
        if status != 0:
            return -1

        return 0

    def run(self):
        self._terminate_event.clear()

        while True:
            if self._terminate_event.isSet():
                break
            if not self._stop_event.isSet():
                # save current filter
                old_filter = self.sock.getsockopt(bluez.SOL_HCI, bluez.HCI_FILTER, 14)

                # perform a device inquiry on bluetooth device #0
                # The inquiry should last 8 * 1.28 = 10.24 seconds
                # before the inquiry is performed, bluez should flush its cache of
                # previously discovered devices
                flt = bluez.hci_filter_new()
                bluez.hci_filter_all_events(flt)
                bluez.hci_filter_set_ptype(flt, bluez.HCI_EVENT_PKT)
                self.sock.setsockopt(bluez.SOL_HCI, bluez.HCI_FILTER, flt)

                duration = 4
                max_responses = 255
                cmd_pkt = struct.pack("BBBBB", 0x33, 0x8b, 0x9e, duration, max_responses)
                bluez.hci_send_cmd(self.sock, bluez.OGF_LINK_CTL, bluez.OCF_INQUIRY, cmd_pkt)

                results = []

                done = False
                while not done:
                    pkt = self.sock.recv(255)
                    ptype, event, plen = struct.unpack("BBB", pkt[:3])
                    if event == bluez.EVT_INQUIRY_RESULT_WITH_RSSI:
                        pkt = pkt[3:]
                        nrsp = bluetooth.get_byte(pkt[0])
                        for i in range(nrsp):
                            addr = bluez.ba2str(pkt[1 + 6 * i:1 + 6 * i + 6])
                            rssi = bluetooth.byte_to_signed_int(bluetooth.get_byte(pkt[1 + 13 * nrsp + i]))
                            print "Dedicated: ", self.dedicated
                            print "Found: ", addr
                            if self.dedicated is None or addr == self.dedicated:
                                results.append((addr, rssi))
                                self._bt_queue.put((addr, rssi))
                            # print "Device found."
                            # print("Mac Address : [%s], Signal Strength: [%d]" % (addr, rssi))

                    elif event == bluez.EVT_INQUIRY_COMPLETE:
                        done = True
                    elif event == bluez.EVT_CMD_STATUS:
                        status, ncmd, opcode = struct.unpack("BBH", pkt[3:7])
                        if status != 0:
                            print("uh oh...")
                            self.printpacket(pkt[3:7])
                            done = True
                    elif event == bluez.EVT_INQUIRY_RESULT:
                        pkt = pkt[3:]
                        nrsp = bluetooth.get_byte(pkt[0])
                        for i in range(nrsp):
                            addr = bluez.ba2str(pkt[1 + 6 * i:1 + 6 * i + 6])
                            results.append((addr, -1))

                # restore old filter
                self.sock.setsockopt(bluez.SOL_HCI, bluez.HCI_FILTER, old_filter)

                if len(results) > 0:
                    print results
            else:
                time.sleep(1)

if __name__ == '__main__':

    ctrl = BTCtrl()
    ctrl.initialize()
    re = ctrl.set_inquiry_mode()
    if re != 0:
        sys.exit(0)

    while True:
        ctrl.run()

